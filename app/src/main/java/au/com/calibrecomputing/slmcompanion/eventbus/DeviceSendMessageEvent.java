package au.com.calibrecomputing.slmcompanion.eventbus;

import au.com.calibrecomputing.slmcompanion.settings.Message;

/**
 * Event bus event happen when have new message received from device
 */
public class DeviceSendMessageEvent {

    private final Message message;

    public DeviceSendMessageEvent(Message message) {
        this.message = message;
    }

    public Message message() {
        return message;
    }
}
