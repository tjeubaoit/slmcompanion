package au.com.calibrecomputing.slmcompanion.transport;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Process;
import android.os.SystemClock;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import au.com.calibrecomputing.slmcompanion.common.Logger;
import au.com.calibrecomputing.slmcompanion.eventbus.ConnectStatusEvent;
import au.com.calibrecomputing.slmcompanion.eventbus.DeviceSendMessageEvent;
import au.com.calibrecomputing.slmcompanion.settings.Message;
import au.com.calibrecomputing.slmcompanion.settings.MessageSerialization;
import au.com.calibrecomputing.slmcompanion.settings.ParseMessageException;

public class BluetoothService implements TransportService {

    private static final UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    // delay between write message in milliseconds
    private static final long WRITE_DELAY = 200L;

    private final Logger logger = Logger.getLogger(this.getClass());
    private Handler handler;
    private HandlerThread handlerThread;

    private BluetoothThread bluetoothThread;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothDevice bluetoothDevice;

    public BluetoothService() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        handlerThread = new HandlerThread("Bluetooth Background Thread", Process.THREAD_PRIORITY_BACKGROUND);
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    @Override
    public void connect(RemoteDevice device) {
        bluetoothDevice = bluetoothAdapter.getRemoteDevice(device.address());

        // remove all in-queue callbacks
        handler.removeCallbacksAndMessages(null);

        handler.post(new Runnable() {
            @Override
            public void run() {
                doConnect();
            }
        });
    }

    @Override
    public void disconnect() {
        if (bluetoothThread != null) {
            bluetoothThread.cancel();
        }
    }

    @Override
    public void send(final Message message) {
        // send message in background thread to avoid block UI
        handler.post(new Runnable() {
            @Override
            public void run() {
                doSend(MessageSerialization.serialize(message));
            }
        });
    }

    @Override
    public void close() {
        disconnect();
        handlerThread.quit();
    }

    private void reconnect(long delay) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Reconnect to last connected device
                if (bluetoothDevice != null) {
                    logger.info("Try reconnect to " + bluetoothDevice.getName());
                    doConnect();
                }
            }
        }, delay);
    }

    private void doConnect() {
        if (bluetoothThread != null) {
            bluetoothThread.cancel();
        }

        BluetoothSocket socket = null;
        try {
            // Get a BluetoothSocket to connect with the given BluetoothDevice.
            socket = bluetoothDevice.createRfcommSocketToServiceRecord(SPP_UUID);

            // Cancel all discovery to avoid slow connection
            bluetoothAdapter.cancelDiscovery();

            // Connect to the remote device through the socket. This call blocks
            // until it succeeds or throws an exception.
            socket.connect();

            bluetoothThread = new BluetoothThread(socket);
            bluetoothThread.start();

            sendEvent(ConnectStatusEvent.STATUS_CONNECTED);
        } catch (IOException e) {
            logger.error("Unable to connect", e);
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e1) {
                    logger.warn("Could not close the client socket", e1);
                }
            }
        }
    }

    private void doSend(String data) {
        if (bluetoothThread != null) bluetoothThread.write(data);
    }

    private void sendEvent(Object event) {
        EventBus.getDefault().post(event);
    }

    private class BluetoothThread extends Thread {

        private AtomicBoolean running = new AtomicBoolean(true);

        private BluetoothSocket socket;
        private InputStream inputStream;
        private OutputStream outputStream;

        public BluetoothThread(BluetoothSocket socket) throws IOException {
            this.socket = socket;
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
        }

        @Override
        public void run() {
            String strMessage;
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

            // Keep listening to the InputStream until an exception occurs or user cancel connection
            while (running.get()) {
                try {
                    while ((strMessage = br.readLine()) != null) {
                        // We have a message so send it to the UI
                        try {
                            Message message = MessageSerialization.deserializer(strMessage);
                            sendEvent(new DeviceSendMessageEvent(message));
                        } catch (ParseMessageException e) {
                            logger.warn("Parse message error: " + strMessage, e);
                        }
                    }
                } catch (IOException e) {
                    sendEvent(ConnectStatusEvent.STATUS_DISCONNECTED);
                    break;
                }
            }
        }

        public void write(String input) {
            byte[] bytes = input.getBytes();
            try {
                outputStream.write(bytes);
                SystemClock.sleep(WRITE_DELAY);
            } catch (IOException e) {
                logger.error("Write failed", e);
            }
        }

        /**
         * Closes the client socket and causes the thread to finish.
         */
        public void cancel() {
            if (!running.get()) return;
            running.set(false);
            try {
                socket.close();
            } catch (IOException e) {
                logger.warn("Could not close the connect socket", e);
            }
        }
    }
}
