package au.com.calibrecomputing.slmcompanion.settings;

import au.com.calibrecomputing.slmcompanion.common.Utils;

public class MessageSerialization {

    public static Message deserializer(String input) {
        String key, value;
        try {
            if (input.contains(":")) {
                key = input.substring(0, 3);
                value = input.substring(4).trim();
            } else if (input.contains(",")) {
                key = input.substring(0, 2);
                value = input.substring(3).trim();
            } else throw new IllegalArgumentException("Illegal message format");

            String simplifyKey = key.replaceAll("[0-9]", "");
            switch (simplifyKey) {
                case Message.SHIFT_POINT:
                    return new ShiftPointMessage(key, value);
                case Message.SPEED_WARN:
                    return new SpeedWarnMessage(key, value);
                default:
                    return new SimpleMessage(key, value);
            }
        } catch (Exception e) {
            throw new ParseMessageException(e);
        }
    }

    public static String serialize(Message msg) {
        switch (msg.type()) {
            case Message.SHIFT_POINT:
            case Message.SPEED_WARN:
                ShiftPointMessage spm = (ShiftPointMessage) msg;
                return Utils.formatString("%s,%d,%s\n", spm.type(), spm.slot(), spm.value());

            default:
                return Utils.formatString("%s,%s\n", msg.key(), msg.value());
        }
    }
}
