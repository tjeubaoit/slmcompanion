package au.com.calibrecomputing.slmcompanion.transport;

import java.io.Closeable;

import au.com.calibrecomputing.slmcompanion.settings.Message;

public interface TransportService extends Closeable {

    void connect(RemoteDevice device);

    void disconnect();

    void send(Message message);

    void close();
}
