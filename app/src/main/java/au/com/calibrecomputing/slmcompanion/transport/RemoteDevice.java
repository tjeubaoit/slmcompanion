package au.com.calibrecomputing.slmcompanion.transport;

import android.bluetooth.BluetoothDevice;

public class RemoteDevice {

    public static RemoteDevice bluetooth(BluetoothDevice device) {
        return new RemoteDevice(device.getName(), device.getAddress());
    }

    private final String name;
    private final String address;

    public RemoteDevice(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String name() {
        return this.name;
    }

    public String address() {
        return this.address;
    }
}
