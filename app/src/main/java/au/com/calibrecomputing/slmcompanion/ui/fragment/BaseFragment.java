package au.com.calibrecomputing.slmcompanion.ui.fragment;

import android.os.Handler;
import android.support.v4.app.Fragment;

import org.greenrobot.eventbus.EventBus;

import java.util.Collection;

import au.com.calibrecomputing.slmcompanion.common.Logger;
import au.com.calibrecomputing.slmcompanion.eventbus.UserChangeSettingEvent;
import au.com.calibrecomputing.slmcompanion.settings.Message;

public abstract class BaseFragment extends Fragment {

    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     * When user make some setting changes, call this method to notify changes to
     * main activity to update current settings
     * @param message the message identify for
     */
    public final void sendMessageEvent(Message message) {
        EventBus.getDefault().post(new UserChangeSettingEvent(message));
    }

    /**
     * Update UI with set of messages. Post by a {@link android.os.Handler}
     * with some delay to improve performance
     * @param messages set of messages sent by device
     */
    public final void updateUI(final Collection<Message> messages) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doUpdateUI(messages);
            }
        }, 500);
    }

    /**
     * Real implementation of {@link BaseFragment#updateUI(Collection)}
     */
    protected abstract void doUpdateUI(Collection<Message> messages);

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
