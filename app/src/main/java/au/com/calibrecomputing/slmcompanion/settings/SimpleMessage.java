package au.com.calibrecomputing.slmcompanion.settings;

import java.io.Serializable;

public class SimpleMessage implements Message, Serializable {

    private final String key;
    private final String value;

    public SimpleMessage(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String type() {
        return this.key;
    }

    @Override
    public String key() {
        return this.key;
    }

    public String value() {
        return this.value;
    }

    @Override
    public String toString() {
        return key() + ":" + value();
    }
}
