package au.com.calibrecomputing.slmcompanion.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import au.com.calibrecomputing.slmcompanion.R;
import au.com.calibrecomputing.slmcompanion.ui.listener.OnEditTextActionDone;

public class ShiftPointsAdapter extends RecyclerView.Adapter<ShiftPointsAdapter.ViewHolder> {

    private final Context context;
    private final List<ShiftPoint> shiftPoints;
    private final OnItemValueChangeListener listener;

    public ShiftPointsAdapter(Context context, List<ShiftPoint> shiftPoints,
                              OnItemValueChangeListener listener) {
        this.shiftPoints = shiftPoints;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_shift_points, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tvTitle.setText(shiftPoints.get(position).title);
        holder.edtValue.setText(shiftPoints.get(position).value);
        holder.edtValue.setOnEditorActionListener(new OnEditTextActionDone() {
            @Override
            public void onActionDone(TextView textView, String text, KeyEvent keyEvent) {
                shiftPoints.get(holder.getAdapterPosition()).value = text;
                if (listener != null) {
                    listener.onItemValueChange(holder.getAdapterPosition(), text);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return shiftPoints.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle;
        private EditText edtValue;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_sp_title);
            edtValue = (EditText) itemView.findViewById(R.id.edt_sp_value);
        }
    }

    public static class ShiftPoint {
        private String value;
        private String title;

        public ShiftPoint(String title, String value) {
            this.title = title;
            this.value = value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public interface OnItemValueChangeListener {
        void onItemValueChange(int pos, String value);
    }
}