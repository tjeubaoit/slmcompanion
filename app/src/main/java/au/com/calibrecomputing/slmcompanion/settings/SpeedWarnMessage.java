package au.com.calibrecomputing.slmcompanion.settings;

import java.io.Serializable;

public class SpeedWarnMessage extends ShiftPointMessage implements Serializable {

    public SpeedWarnMessage(String key, String value) {
        super(key, value);
    }

    public SpeedWarnMessage(int slot, String value) {
        this(SPEED_WARN + slot, value);
    }

    @Override
    public String type() {
        return SPEED_WARN;
    }
}
