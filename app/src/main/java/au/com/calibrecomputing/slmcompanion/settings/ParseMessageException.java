package au.com.calibrecomputing.slmcompanion.settings;

public class ParseMessageException extends RuntimeException {

    public ParseMessageException() {
    }

    public ParseMessageException(String message) {
        super(message);
    }

    public ParseMessageException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParseMessageException(Throwable cause) {
        super(cause);
    }
}
