package au.com.calibrecomputing.slmcompanion.eventbus;

import au.com.calibrecomputing.slmcompanion.settings.Settings;

/**
 * Event bus event happen when the settings has been loaded
 * from a xml file successfully
 */
public class SettingLoadedEvent {

    private final Settings settings;

    public SettingLoadedEvent(Settings settings) {
        this.settings = settings;
    }

    public Settings settings() {
        return this.settings;
    }
}
