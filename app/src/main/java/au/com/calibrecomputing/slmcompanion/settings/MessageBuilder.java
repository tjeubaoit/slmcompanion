package au.com.calibrecomputing.slmcompanion.settings;

public class MessageBuilder {

    public static Message shiftPointMessage(int slot, String rpm) {
        return new ShiftPointMessage(slot, rpm);
    }

    public static Message speedWarnMessage(int slot, String speed) {
        return new SpeedWarnMessage(slot, speed);
    }

    public static Message flashPeriodMessage(String periodInMillis) {
        return new SimpleMessage(Message.FLASH_PERIOD, periodInMillis);
    }

    public static Message brightnessMessage(String brightness) {
        return new SimpleMessage(Message.BRIGHTNESS, brightness);
    }

    public static Message testModeMessage() {
        return new SimpleMessage(Message.TEST_MODE, "");
    }

    public static Message requestConfigMessage() {
        return new SimpleMessage(Message.REQUEST_CONFIG, "");
    }
}
