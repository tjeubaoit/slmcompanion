package au.com.calibrecomputing.slmcompanion.settings;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class Settings implements Serializable {

    public static final String ZERO = "0";

    private static final String NODE_SHIFT_POINTS = "ShiftPoints";
    private static final String NODE_SHIFT_POINT = "SP";
    private static final String NODE_LED_PATTERNS = "LEDPatterns";
    private static final String NODE_LED_PATTERN = "LP";
    private static final String NODE_SETTINGS = "Settings";
    private static final String NODE_BRIGHTNESS = "Brightness";
    private static final String NODE_FLASH_PERIOD = "FlashPeriod";
    private static final String NODE_FLASH = "Flash";

    private final Map<String, Message> messageMap = new LinkedHashMap<>();

    private Settings() {
        // init default values
        update(MessageBuilder.brightnessMessage(ZERO));
        update(MessageBuilder.flashPeriodMessage(ZERO));

        for (int i = 1; i <= 4; i++) {
            update(MessageBuilder.shiftPointMessage(i, ZERO));
        }
        for (int i = 0; i <= 9; i++) {
            update(MessageBuilder.speedWarnMessage(i, ZERO));
        }
    }

    public void init(Settings settings) {
        this.messageMap.putAll(settings.messageMap);
    }

    public void update(Message msg) {
        messageMap.put(msg.key(), msg);
    }

    public Message findMessage(String key) {
        return messageMap.get(key);
    }

    public Collection<Message> messages() {
        return messageMap.values();
    }

    /**
     * @return new default empty Settings object
     */
    public static Settings empty() {
        return new Settings();
    }

    public static void saveToXml(String fileName, Settings settings) throws IOException {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document doc = builder.newDocument();
            Element rootElement = doc.createElement("NewDataSet");
            doc.appendChild(rootElement);

            Element shiftPointsElement = doc.createElement("ShiftPoints");
            Element settingsElement = doc.createElement("Settings");
            rootElement.appendChild(shiftPointsElement);
            rootElement.appendChild(settingsElement);

            for (int i = 0; i <= 13; i++) {
                Element element = doc.createElement(Message.SHIFT_POINT + i);
                if (i < 4) { // shift points
                    Message msg = settings.findMessage(Message.SHIFT_POINT + (i + 1));
                    element.setTextContent(msg.value());
                    shiftPointsElement.appendChild(element);
                } else { // speed warn points
                    Message msg = settings.findMessage(Message.SPEED_WARN + (i - 4));
                    element.setTextContent(msg.value());
                    shiftPointsElement.appendChild(element);
                }
            }

            // brightness element
            Element brightness = doc.createElement("Brightness");
            brightness.setTextContent(settings.findMessage(Message.BRIGHTNESS).value());

            // flash period element
            Element flashPeriod = doc.createElement("FlashPeriod");
            flashPeriod.setTextContent(settings.findMessage(Message.FLASH_PERIOD).value());

            settingsElement.appendChild(brightness);
            settingsElement.appendChild(flashPeriod);

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(fileName));
            transformer.transform(source, result);
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    public static Settings loadFromXml(String fileName) throws IOException {
        Settings settings = Settings.empty();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document doc = builder.parse(new File(fileName));
            doc.getDocumentElement().normalize();

            // load settings table
            Node settingNode = doc.getElementsByTagName(NODE_SETTINGS).item(0);
            NodeList settingNodeList = settingNode.getChildNodes();
            for (int i = 0; i < settingNodeList.getLength(); i++) {
                Node node = settingNodeList.item(i);
                if (node.getNodeType() != Node.ELEMENT_NODE) continue;

                switch (node.getNodeName()) {
                    case NODE_BRIGHTNESS:
                        settings.update(MessageBuilder.brightnessMessage(node.getTextContent()));
                        break;
                    case NODE_FLASH_PERIOD:
                        settings.update(MessageBuilder.flashPeriodMessage(node.getTextContent()));
                        break;
                    default:
                        break;
                }
            }

            // load shift points table
            Node spNode = doc.getElementsByTagName(NODE_SHIFT_POINTS).item(0);
            NodeList spNodeList = spNode.getChildNodes();
            for (int i = 0; i < spNode.getChildNodes().getLength(); i++) {
                Node node = spNodeList.item(i);
                if (node.getNodeType() != Node.ELEMENT_NODE) continue;

                String nodeName = node.getNodeName();
                if (!nodeName.startsWith(NODE_SHIFT_POINT)) continue;

                int spIndex = Integer.parseInt(nodeName.substring(2));
                if (spIndex >= 0 && spIndex < 4) {
                    settings.update(MessageBuilder.shiftPointMessage(spIndex + 1, node.getTextContent()));
                } else if (spIndex >= 4 && spIndex <= 13) {
                    settings.update(MessageBuilder.speedWarnMessage(spIndex - 4, node.getTextContent()));
                }
            }
        } catch (Exception e) {
            throw new IOException(e);
        }

        return settings;
    }
}
