package au.com.calibrecomputing.slmcompanion.ui;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import au.com.calibrecomputing.slmcompanion.BuildConfig;
import au.com.calibrecomputing.slmcompanion.R;

public class BluetoothConnectActivity extends AppCompatActivity {

    private Button btnSearch;
    private View progressView;
    private BluetoothDeviceAdapter listDevicesAdapter;

    private BluetoothAdapter bluetoothAdapter;

    // Create a BroadcastReceiver for ACTION_FOUND and ACTION_DISCOVERY_FINISHED.
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                listDevicesAdapter.add(device);
                listDevicesAdapter.notifyDataSetChanged();
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                cancelDiscovery();
            }
        }
    };

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_connect);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listDevicesAdapter = new BluetoothDeviceAdapter(this);
        ListView listView = (ListView) findViewById(R.id.list_devices);
        listView.setAdapter(listDevicesAdapter);
        listView.setEmptyView(findViewById(R.id.txt_empty));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
                setResultAndReturn(listDevicesAdapter.getItem(pos));
            }
        });

        btnSearch = (Button) findViewById(R.id.button_search_devices);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!bluetoothAdapter.isDiscovering()) {
                    startDiscovery();
                } else {
                    cancelDiscovery();
                }
            }
        });
        progressView = findViewById(R.id.progress);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        listDevicesAdapter.addAll(bluetoothAdapter.getBondedDevices());
        startDiscovery();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Register for broadcasts when a device is discovered
        registerReceiver(receiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));

        // Register for broadcasts when discovery finished
        registerReceiver(receiver, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
        cancelDiscovery();
    }

    private void startDiscovery() {
        bluetoothAdapter.startDiscovery();
        btnSearch.setText(android.R.string.cancel);
        progressView.setVisibility(View.VISIBLE);
    }

    private void cancelDiscovery() {
        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }
        btnSearch.setText(R.string.action_search_devices);
        progressView.setVisibility(View.INVISIBLE);
    }

    private void setResultAndReturn(BluetoothDevice selectedDevice) {
        Intent i = new Intent();
        i.putExtra(BluetoothDevice.EXTRA_DEVICE, selectedDevice);
        setResult(RESULT_OK, i);
        finish();
    }

    static class BluetoothDeviceAdapter extends ArrayAdapter<BluetoothDevice> {

        public BluetoothDeviceAdapter(@NonNull Context context) {
            super(context, R.layout.item_bluetooth_device);
        }

        @Override
        public void add(@Nullable BluetoothDevice item) {
            for (int i = 0; i < getCount(); i++) {
                if (getItem(i).getAddress().equals(item.getAddress())) return;
                if (!BuildConfig.DEBUG && !item.getName().toLowerCase().startsWith("shift")) return;
            }
            super.add(item);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = LayoutInflater.from(getContext()).inflate(R.layout.item_bluetooth_device, parent, false);
            }

            BluetoothDevice device = getItem(position);
            TextView tvName = (TextView) v.findViewById(R.id.text_name);
            TextView tvAddress = (TextView) v.findViewById(R.id.text_address);

            if (device.getName() != null) tvName.setText(device.getName());
            tvAddress.setText(device.getAddress());

            return v;
        }
    }

}
