package au.com.calibrecomputing.slmcompanion.settings;

import java.io.Serializable;

public class ShiftPointMessage extends SimpleMessage implements Serializable {

    private final int slot;

    public ShiftPointMessage(String key, String value) {
        super(key, value);
        this.slot = Integer.parseInt(key.substring(2));
    }

    public ShiftPointMessage(int slot, String value) {
        super(SHIFT_POINT + slot, value);
        this.slot = slot;
    }

    public int slot() {
        return this.slot;
    }

    @Override
    public String type() {
        return SHIFT_POINT;
    }
}
