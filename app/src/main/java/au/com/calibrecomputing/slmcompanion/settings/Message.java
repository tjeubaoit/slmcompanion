package au.com.calibrecomputing.slmcompanion.settings;

public interface Message {

    String VEHICLE_RPM = "RPM";
    String VEHICLE_SPEED = "SPD";
    String SHIFT_POINT = "SP";
    String SPEED_WARN = "SW";
    String BRIGHTNESS = "BRI";
    String STATUS = "MSG";
    String FLASH_PERIOD = "FPD";
    String TEST_MODE = "TM";
    String REQUEST_CONFIG = "SP?";

    /**
     * <i>type</i> same as <i>key</i> except when message is shift points or speed warn message,
     * eg: "RPM: 450", key = RPM, "SP1: 100", key = SP, "SW3: 242", key = SW
     * @return type message
     */
    String type();

    /**
     * Key to identify message meaning
     * eg: "RPM: 450", key = RPM, "SP1: 100", key = SP1, "SW3: 242", key = SW3
     * @return key of message
     */
    String key();

    /**
     * @return value of message
     */
    String value();

}
