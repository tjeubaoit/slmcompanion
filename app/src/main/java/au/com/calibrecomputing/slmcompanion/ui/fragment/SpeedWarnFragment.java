package au.com.calibrecomputing.slmcompanion.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import au.com.calibrecomputing.slmcompanion.R;
import au.com.calibrecomputing.slmcompanion.eventbus.DeviceSendMessageEvent;
import au.com.calibrecomputing.slmcompanion.eventbus.SettingLoadedEvent;
import au.com.calibrecomputing.slmcompanion.settings.Message;
import au.com.calibrecomputing.slmcompanion.settings.MessageBuilder;
import au.com.calibrecomputing.slmcompanion.settings.Settings;
import au.com.calibrecomputing.slmcompanion.settings.SpeedWarnMessage;
import au.com.calibrecomputing.slmcompanion.ui.adapter.ShiftPointsAdapter;

public class SpeedWarnFragment extends BaseFragment implements ShiftPointsAdapter.OnItemValueChangeListener {

    private static final int NUM_SPEED_WARN_POINTS = 10;

    private ShiftPointsAdapter adapter;
    private List<ShiftPointsAdapter.ShiftPoint> speedWarnPoints = new ArrayList<>(NUM_SPEED_WARN_POINTS);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (speedWarnPoints.isEmpty()) {
            for (int i = 0; i < NUM_SPEED_WARN_POINTS; i++) {
                String title = getString(R.string.speed_warn, i + 1);
                String value = Settings.ZERO;
                speedWarnPoints.add(new ShiftPointsAdapter.ShiftPoint(title, value));
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_speed_warn, container, false);

        adapter = new ShiftPointsAdapter(getContext(), speedWarnPoints, this);
        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.rv_speed_warn_points);
        recyclerView.setAdapter(adapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        return v;
    }

    @Override
    public void onItemValueChange(int pos, String value) {
        sendMessageEvent(MessageBuilder.speedWarnMessage(pos, value));
    }

    @Override
    public void doUpdateUI(Collection<Message> messages) {
        for (Message message : messages) {
            handleMessage(message);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDeviceSendMessage(DeviceSendMessageEvent event) {
        handleMessage(event.message());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSettingLoaded(SettingLoadedEvent event) {
        for (Message message : event.settings().messages()) {
            handleMessage(message);
        }
    }

    private void handleMessage(Message message) {
        if (!Message.SPEED_WARN.equals(message.type())) return;

        SpeedWarnMessage swMessage = (SpeedWarnMessage) message;
        int pos = swMessage.slot();
        if (pos >= 0 && pos < NUM_SPEED_WARN_POINTS) {
            speedWarnPoints.get(pos).setValue(swMessage.value());
            adapter.notifyItemChanged(pos);
        }
    }
}
