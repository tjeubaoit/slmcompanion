package au.com.calibrecomputing.slmcompanion.service;

import android.app.IntentService;
import android.content.Intent;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import au.com.calibrecomputing.slmcompanion.R;
import au.com.calibrecomputing.slmcompanion.common.Logger;
import au.com.calibrecomputing.slmcompanion.common.Utils;
import au.com.calibrecomputing.slmcompanion.eventbus.SettingLoadedEvent;
import au.com.calibrecomputing.slmcompanion.settings.Settings;


public class IOService extends IntentService {

    public static final String ACTION_LOAD_CONFIG = "ACTION_LOAD_CONFIG";
    public static final String ACTION_SAVE_CONFIG = "ACTION_SAVE_CONFIG";

    public static final String EXTRA_FILE_NAME = "EXTRA_FILE_NAME";
    public static final String EXTRA_SETTINGS = "EXTRA_SETTINGS";

    private final Logger logger = Logger.getLogger(this.getClass());

    public IOService() {
        super("IOService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_LOAD_CONFIG.equals(action)) {
                final String fileName = intent.getStringExtra(EXTRA_FILE_NAME);
                handleOpenConfig(fileName);
            } else if (ACTION_SAVE_CONFIG.equals(action)) {
                final String fileName = intent.getStringExtra(EXTRA_FILE_NAME);
                final Settings settings = (Settings) intent.getSerializableExtra(EXTRA_SETTINGS);
                handleSaveConfig(fileName, settings);
            }
        }
    }

    private void handleOpenConfig(String fileName) {
        try {
            logger.info("Load config at file " + fileName);
            EventBus.getDefault().post(new SettingLoadedEvent(Settings.loadFromXml(fileName)));
        } catch (IOException e) {
            Utils.showToast(getApplicationContext(), getString(R.string.load_config_fail));
            logger.error(e.getMessage(), e);
        }
    }

    private void handleSaveConfig(String fileName, Settings settings) {
        try {
            logger.info(Utils.formatString("Save config {%s} to file %s",
                    Utils.join(settings.messages(), ","), fileName));
            Settings.saveToXml(fileName, settings);
            Utils.showLongToast(getApplicationContext(), getString(R.string.save_config_success, fileName));
        } catch (IOException e) {
            Utils.showToast(getApplicationContext(), getString(R.string.save_config_fail));
            logger.error(e.getMessage(), e);
        }
    }
}
