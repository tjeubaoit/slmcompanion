package au.com.calibrecomputing.slmcompanion.ui;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import au.com.calibrecomputing.slmcompanion.R;
import au.com.calibrecomputing.slmcompanion.common.Logger;
import au.com.calibrecomputing.slmcompanion.common.Utils;
import au.com.calibrecomputing.slmcompanion.eventbus.ConnectStatusEvent;
import au.com.calibrecomputing.slmcompanion.eventbus.DeviceSendMessageEvent;
import au.com.calibrecomputing.slmcompanion.eventbus.SettingLoadedEvent;
import au.com.calibrecomputing.slmcompanion.eventbus.UserChangeSettingEvent;
import au.com.calibrecomputing.slmcompanion.service.IOService;
import au.com.calibrecomputing.slmcompanion.settings.Message;
import au.com.calibrecomputing.slmcompanion.settings.MessageBuilder;
import au.com.calibrecomputing.slmcompanion.settings.Settings;
import au.com.calibrecomputing.slmcompanion.transport.BluetoothService;
import au.com.calibrecomputing.slmcompanion.transport.RemoteDevice;
import au.com.calibrecomputing.slmcompanion.transport.TransportService;
import au.com.calibrecomputing.slmcompanion.ui.fragment.BaseFragment;
import au.com.calibrecomputing.slmcompanion.ui.fragment.BasicSettingsFragment;
import au.com.calibrecomputing.slmcompanion.ui.fragment.ShiftPointsFragment;
import au.com.calibrecomputing.slmcompanion.ui.fragment.SpeedWarnFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_SELECT_BT_DEVICE = 2;
    private static final int REQUEST_CHOOSE_FILE = 3;

    private static final String PREFS_LAST_CONNECTED_DEVICE = "last.connected.device";
    private static final String KEY_DEVICE_ADDRESS = "device.address";

    private static final int NUM_TAB = 3;
    private static final int TAB_BASIC = 0;
    private static final int TAB_SHIFT_POINTS = 1;
    private static final int TAB_SPEED_WARN = 2;

    private final Logger logger = Logger.getLogger(this.getClass());
    private BluetoothAdapter bluetoothAdapter;
    private TransportService transportService;
    private RemoteDevice currentDevice;
    private boolean backFromActivityResultFlag = false;
    private boolean forwardToActivityResultFlag = false;

    // keep references to all fragments
    private final List<BaseFragment> fragments = new ArrayList<>(NUM_TAB);
    private int currentTabId = TAB_BASIC;

    private final Settings settings = Settings.empty();

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initNavigationDrawer();
        initBottomNavigationView();
        initTabs();

        initTransport();
    }

    private void initNavigationDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initBottomNavigationView() {
        final BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_basic_settings:
                        changeTab(TAB_BASIC);
                        return true;
                    case R.id.navigation_shift_points:
                        changeTab(TAB_SHIFT_POINTS);
                        return true;
                    case R.id.navigation_speed_warn:
                        changeTab(TAB_SPEED_WARN);
                        return true;
                }
                return false;
            }
        });
    }

    private void initTransport() {
        transportService = new BluetoothService();
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    private void initTabs() {
        // init fragments and bottom navigation views
        fragments.add(new BasicSettingsFragment());
        fragments.add(new ShiftPointsFragment());
        fragments.add(new SpeedWarnFragment());

        changeTab(TAB_BASIC);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_discovery_bluetooth) {
            selectBluetoothDevice();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_open_config) {
            selectFileToLoadConfig();
        } else if (id == R.id.nav_save_config) {
            saveConfigToFile();
        } else if (id == R.id.nav_send_config) {
            sendConfigToDevice();
        } else if (id == R.id.nav_retrieve_config) {
            loadConfigFromDevice();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);

        // Device does not support Bluetooth
        if (bluetoothAdapter == null) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.error)
                    .setMessage(R.string.device_does_not_support_bluetooth)
                    .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Process.killProcess(Process.myPid());
                            System.exit(1);
                        }
                    }).show();
            return;
        }

        // Bluetooth disabled
        if (!bluetoothAdapter.isEnabled()) {
            requestEnableBluetooth();
            return;
        }

        if (!backFromActivityResultFlag) {
            tryConnectToLastDevice();
        } else {
            backFromActivityResultFlag = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);

        if (!forwardToActivityResultFlag) {
            transportService.disconnect();
        } else {
            forwardToActivityResultFlag = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        transportService.close();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        backFromActivityResultFlag = true;
        if (resultCode != RESULT_OK) {
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                tryConnectToLastDevice();
                break;
            case REQUEST_SELECT_BT_DEVICE:
                BluetoothDevice bluetoothDevice = data.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                connectToDevice(RemoteDevice.bluetooth(bluetoothDevice));
                break;
            case REQUEST_CHOOSE_FILE:
                String path = data.getData().getPath();
                loadConfigFromFile(path);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        forwardToActivityResultFlag = true;
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, @Nullable Bundle options) {
        forwardToActivityResultFlag = true;
        super.startActivityForResult(intent, requestCode, options);
    }

    private void selectFileToLoadConfig() {
        final Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.setType("file/*");
        i.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(i, getString(R.string.file_name)), REQUEST_CHOOSE_FILE);
    }

    private void loadConfigFromFile(String path) {
        Intent i = new Intent(MainActivity.this, IOService.class);
        i.putExtra(IOService.EXTRA_FILE_NAME, path);
        i.setAction(IOService.ACTION_LOAD_CONFIG);
        startService(i);
    }

    private void saveConfigToFile() {
        // show dialog to choose where to save config file
        final EditText input = new EditText(this);
        final String defaultName = "slm_settings_" + Utils.formatDate(new Date(), "yyyy-MM-dd");

        input.setText(defaultName);
        input.setSelection(0, input.getText().length());
        input.requestFocus(View.FOCUS_LEFT);
        input.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(input, 0);
            }
        }, 300);

        new AlertDialog.Builder(this)
                .setTitle(R.string.input_filename)
                .setView(input)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String extension = ".xml";
                        String fileName = input.getText().toString() + extension;
                        String rootPath = Environment.getExternalStorageState() + "/"
                                + getString(R.string.app_name).replace(" ", "");
                        File rootDir = new File(rootPath);
                        if (!rootDir.exists()) rootDir.mkdir();

                        Intent i = new Intent(MainActivity.this, IOService.class);
                        i.putExtra(IOService.EXTRA_FILE_NAME, rootPath + "/" + fileName);
                        i.putExtra(IOService.EXTRA_SETTINGS, settings);
                        i.setAction(IOService.ACTION_SAVE_CONFIG);
                        startService(i);
                    }
                })
                .show();
    }

    private void sendConfigToDevice() {
        for (Message message : settings.messages()) {
            transportService.send(message);
        }
    }

    private void loadConfigFromDevice() {
        // request device current configuration
        transportService.send(MessageBuilder.requestConfigMessage());
    }

    /**
     * Start activity to select a paired device or discovery new device to connect
     */
    private void selectBluetoothDevice() {
        Intent i = new Intent(MainActivity.this, BluetoothConnectActivity.class);
        startActivityForResult(i, REQUEST_SELECT_BT_DEVICE);
    }

    /**
     * Request user to enable bluetooth
     */
    private void requestEnableBluetooth() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }

    /**
     * Connect to remote device
     *
     * @param device remote device to connect
     */
    private void connectToDevice(RemoteDevice device) {
        getSupportActionBar().setTitle("Connecting...");
        transportService.connect(device);
        currentDevice = device;
    }

    /**
     * Try connect to last connected device if it exist
     */
    private void tryConnectToLastDevice() {
        Map<String, ?> map = Utils.getPrefs(this, PREFS_LAST_CONNECTED_DEVICE);
        if (map.containsKey(KEY_DEVICE_ADDRESS)) {
            String address = map.get(KEY_DEVICE_ADDRESS).toString();
            logger.info("Reconnect to last device: " + address);

            BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(address);
            connectToDevice(RemoteDevice.bluetooth(bluetoothDevice));
        }
    }

    private void changeTab(int tabId) {
        int d = currentTabId - tabId;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (d > 0) {
            ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        } else if (d < 0) {
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        }

        ft.replace(R.id.frag_container, fragments.get(tabId)).commit();

        // update GUI components with latest values
        fragments.get(tabId).updateUI(settings.messages());
        currentTabId = tabId;
    }

    private void setActivityTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnectStatusChange(ConnectStatusEvent event) {
        if (event == ConnectStatusEvent.STATUS_DISCONNECTED) {
            Utils.showToast(this, getString(R.string.disconnected));
            setActivityTitle(getString(R.string.disconnected));
            currentDevice = null;
        } else {
            // update connect status on the UI
            Utils.showToast(this, getString(R.string.connected, currentDevice.name()));
            setActivityTitle(currentDevice.name());

            // save connected device to preferences
            Utils.putPrefs(this, PREFS_LAST_CONNECTED_DEVICE,
                    Utils.mapOf(KEY_DEVICE_ADDRESS, currentDevice.address()));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDeviceSendMessage(DeviceSendMessageEvent event) {
        Message message = event.message();
        if (Message.STATUS.equals(message.type())) { // if is status message
            Utils.showToast(this, message.value());
        } else {
            // save message that used to update associated GUI components in the future
            logger.info("Device send: " + message);
            settings.update(message);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserChangeSetting(UserChangeSettingEvent event) {
        Message message = event.message();
        if (Message.TEST_MODE.equals(message.type())) {
            transportService.send(message);
        } else {
            logger.info("Setting change: " + message);
            settings.update(message);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSettingLoaded(SettingLoadedEvent event) {
        logger.info("Setting loaded");
        settings.init(event.settings());

        // update UI with new settings
        fragments.get(currentTabId).updateUI(settings.messages());
    }
}
