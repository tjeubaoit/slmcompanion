package au.com.calibrecomputing.slmcompanion.eventbus;

import au.com.calibrecomputing.slmcompanion.settings.Message;

/**
 * Event bus event happen when user makes some setting changes
 */
public class UserChangeSettingEvent {

    private final Message message;

    public UserChangeSettingEvent(Message message) {
        this.message = message;
    }

    public Message message() {
        return message;
    }
}
