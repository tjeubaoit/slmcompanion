package au.com.calibrecomputing.slmcompanion.eventbus;

/**
 * Event bus event happen when connection state between
 * the device and the phone changed
 */
public enum ConnectStatusEvent {

    STATUS_CONNECTED,
    STATUS_DISCONNECTED
}
