package au.com.calibrecomputing.slmcompanion.ui.listener;

import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import au.com.calibrecomputing.slmcompanion.common.Utils;

public abstract class OnEditTextActionDone implements TextView.OnEditorActionListener {

    String lastText = "0";

    @Override
    public boolean onEditorAction(final TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            String text = textView.getText().toString();
            // prevent edit text from being empty
            if (Utils.isNullOrEmpty(text)) {
                textView.post(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(lastText);
                    }
                });
                return false;
            }
            if (text.startsWith("0")) {
                text = String.valueOf(Integer.parseInt(text));
                final String tmp = text;
                textView.post(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(tmp);
                    }
                });
            }

            if (!text.equals(lastText)) { // ignore if text did not change
                lastText = text;
                onActionDone(textView, text, keyEvent);
            }
        }
        return false;
    }

    public abstract void onActionDone(TextView textView, String text, KeyEvent keyEvent);
}
