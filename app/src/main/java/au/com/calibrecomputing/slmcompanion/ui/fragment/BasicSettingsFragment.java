package au.com.calibrecomputing.slmcompanion.ui.fragment;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Collection;

import au.com.calibrecomputing.slmcompanion.R;
import au.com.calibrecomputing.slmcompanion.eventbus.DeviceSendMessageEvent;
import au.com.calibrecomputing.slmcompanion.settings.Message;
import au.com.calibrecomputing.slmcompanion.settings.MessageBuilder;
import au.com.calibrecomputing.slmcompanion.settings.Settings;
import au.com.calibrecomputing.slmcompanion.ui.listener.OnEditTextActionDone;

public class BasicSettingsFragment extends BaseFragment {

    private EditText edtRpm, edtSpeed;
    private EditText edtFlashPeriod;
    private SeekBar sbBrightness;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_basic, container, false);

        edtRpm = (EditText) v.findViewById(R.id.edt_rpm);
        edtSpeed = (EditText) v.findViewById(R.id.edt_speed);

        initFlashPeriodUI(v);
        initTestModeUI(v);
        initBrightnessUI(v);

        // set default values for GUI components
        sbBrightness.setProgress(50);
        edtFlashPeriod.setText(Settings.ZERO);
        edtRpm.setText(Settings.ZERO);
        edtSpeed.setText(Settings.ZERO);

        return v;
    }

    private void initFlashPeriodUI(View v) {
        edtFlashPeriod = (EditText) v.findViewById(R.id.edt_flash_period);
        edtFlashPeriod.setOnEditorActionListener(new OnEditTextActionDone() {
            @Override
            public void onActionDone(TextView textView, String text, KeyEvent keyEvent) {
                sendMessageEvent(MessageBuilder.flashPeriodMessage(text));
            }
        });
    }

    private void initTestModeUI(View v) {
        Switch swTestMode = (Switch) v.findViewById(R.id.sw_test_mode);
        swTestMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessageEvent(MessageBuilder.testModeMessage());
            }
        });
    }

    private void initBrightnessUI(View v) {
        final TextView tvBrightness = (TextView) v.findViewById(R.id.tv_brightness);
        sbBrightness = (SeekBar) v.findViewById(R.id.sb_brightness);
        sbBrightness.setMax(100);
        sbBrightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress;

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                progress = i;
                tvBrightness.setText(getString(R.string.brightness, i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                sendMessageEvent(MessageBuilder.brightnessMessage(String.valueOf(progress)));
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDeviceSendMessage(DeviceSendMessageEvent event) {
        handleMessage(event.message());
    }

    private void handleMessage(Message message) {
        switch (message.type()) {
            case Message.VEHICLE_RPM:
                updateRpm(message.value());
                break;
            case Message.VEHICLE_SPEED:
                updateSpeed(message.value());
                break;
            case Message.BRIGHTNESS:
                updateBrightness(message.value());
                break;
            case Message.FLASH_PERIOD:
                updateFlashPeriod(message.value());
                break;
            default:
                break;
        }
    }

    private void updateBrightness(String value) {
        try {
            final int progress = Integer.parseInt(value);
            if (progress < 0 || progress > 100) return;
            sbBrightness.post(new Runnable() {
                @Override
                public void run() {
                    sbBrightness.setProgress(progress);
                }
            });
        } catch (NumberFormatException e) {
            logger.info(e.getMessage(), e);
        }
    }

    private void updateFlashPeriod(String value) {
        edtFlashPeriod.setText(value);
    }

    private void updateRpm(String value) {
        edtRpm.setText(value);
    }

    private void updateSpeed(String value) {
        edtSpeed.setText(value);
    }

    @Override
    public void doUpdateUI(final Collection<Message> messages) {
        for (Message message : messages) {
            handleMessage(message);
        }
    }
}
